const express = require('express');
const helmet = require('helmet');
const logger = require('morgan');
const bodyParser = require('body-parser');
const config = require('./server/config/serverConfig.json');

// Set up the express app
const app = express();

// Put on security helmet
app.use(helmet());

// Log requests to the console.
app.use(logger('dev'));

// Allow requests from localhost
app.use((req, res, next) => {
  res.header("Access-Control-Allow-Origin", config.allowOrigin);
  res.header('Access-Control-Allow-Methods', 'GET, PUT, POST, DELETE, HEAD, OPTIONS');
  res.header("Access-Control-Allow-Headers",
    `Origin, X-Requested-With, Content-Type, Accept, access-control-allow-origin, Authorization`);
  next();
});

// Parse incoming requests data (https://github.com/expressjs/body-parser)
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));

// Require routes into the application
require('./server/routes')(app);

// Setup a default catch-all route that sends back a welcome message in JSON format.
app.get('*', (req, res) => res.status(200).send({
  message: 'Welcome to the beginning of nothingness.',
}));

module.exports = app;