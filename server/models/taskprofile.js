'use strict';
module.exports = (sequelize, DataTypes) => {
  const TaskProfile = sequelize.define('TaskProfile', {
    id: {
      type: DataTypes.INTEGER,
      allowNull: false,
      autoIncrement: true,
      primaryKey: true,
    },
    profile: {
      type: DataTypes.STRING,
      allowNull: false,
      unique: true
    }
  });

  TaskProfile.associate = (models) => {
    TaskProfile.hasMany(models.TaskComponent, {
      foreignKey: 'profileId'
    });
    TaskProfile.hasMany(models.Task, {
      foreignKey: 'profileId'
    });
  }

  return TaskProfile;
};