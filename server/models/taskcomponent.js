'use strict';
module.exports = (sequelize, DataTypes) => {
  const TaskComponent = sequelize.define('TaskComponent', {
    label: {
      type: DataTypes.STRING,
      allowNull: false,
      validate : {
        notEmpty: true
      },
    },
    type: {
      type: DataTypes.STRING,
      allowNull: false,
      validate : {
        notEmpty: true
      }
    },
    required: {
      type: DataTypes.BOOLEAN,
      allowNull: false,
      validate : {
        notEmpty: true
      }
    },
    allowEdit: {
      type: DataTypes.BOOLEAN,
      allowNull: false,
      validate : {
        notEmpty: true
      }
    }
  });

  TaskComponent.associate = (models) => {
    TaskComponent.belongsTo(models.TaskProfile, {
      foreignKey: 'profileId',
      onDelete: 'CASCADE'
    });
    TaskComponent.hasMany(models.TaskValue, {
      foreignKey: 'componentId'
    })
  };

  return TaskComponent;
};