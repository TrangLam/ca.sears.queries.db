'use strict';
module.exports = (sequelize, DataTypes) => {
  const TaskValue = sequelize.define('TaskValue', {
    // taskId: {
    //   type: DataTypes.INTEGER,
    //   onDelete: 'CASCADE',
    //   references: {
    //     model: 'Tasks',
    //     key: 'id'
    //   }
    // },
    // componentId: {
    //   type: DataTypes.INTEGER,
    //   onDelete: 'CASCADE',
    //   references: {
    //     model: 'TaskComponents',
    //     key: 'id'
    //   }
    // },
    value: {
      type: DataTypes.STRING,
      allowNull: true,
    }
  });

  TaskValue.associate = (models) => {
    TaskValue.belongsTo(models.Task, {
      foreignKey: 'taskId',
      onDelete: 'CASCADE'
    });
    TaskValue.belongsTo(models.TaskComponent, {
      foreignKey: 'componentId',
      onDelete: 'CASCADE'
    })
  };

  return TaskValue;
};