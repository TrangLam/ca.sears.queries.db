'use strict';
module.exports = (sequelize, DataTypes) => {
  const Task = sequelize.define('Task', {
    name: {
      type: DataTypes.STRING,
      allowNull: false,
      unique: true,
      validate: {
        notEmpty: true
      }
    },
    // profileId: {
    //   type: DataTypes.INTEGER,
    //   onDelete: 'CASCADE',
    //   references: {
    //     model: 'TaskProfiles',
    //     key: 'id'
    //   }
    // }
  });

  Task.associate = (models) => {
    Task.belongsTo(models.TaskProfile, {
      foreignKey: 'profileId',
      onDelete: 'CASCADE',
    });
  };

  return Task;
};