'use strict';
const models = require('./../models');
const taskProfiles = require('./../initialData/profileSeed').taskProfiles;

module.exports = {
  up: (queryInterface, Sequelize) => {
    const toInsert = taskProfiles.map((item) => {
      return {
        profile: item.profile,
        createdAt: new Date(),
        updatedAt: new Date()
      };
    });
    return queryInterface.bulkInsert('TaskProfiles', toInsert, {});
  },

  down: (queryInterface, Sequelize) => {
    /*
      Add reverting commands here.
      Return a promise to correctly handle asynchronicity.

      Example:
      return queryInterface.bulkDelete('Person', null, {});
    */
    return queryInterface.bulkDelete('TaskProfiles', null, {});
  }
};
