'use strict';
const _ = require('lodash');
const models = require('./../models');
const Components = require('./../initialData/componentSeed').Components;

function prepareComponents(profile, components) {
  const completedComponents = components.map((component) => {
    const { label, type, required, allowEdit } = component;
    return {
      profileId: profile,
      label,
      type,
      required,
      allowEdit,
      createdAt: new Date(),
      updatedAt: new Date()
    };
  });

  return completedComponents;
}

module.exports = {
  up: (queryInterface, Sequelize) => {
    let promises = [];
    const profiles = _.keys(Components);

    profiles.forEach((profile) => {
      const components = Components[profile];

      promises.push(
        models.TaskProfile
          .findOne({
            where: { profile: profile},
            attributes: ['id']
          })
          .then((resultProfile) => {
            const id = resultProfile.id;
            const completedComponents = prepareComponents(id, components);
            return queryInterface.bulkInsert('TaskComponents', completedComponents);
          })
      );
    });

    return Promise.all(promises);
  },

  down: function (queryInterface, Sequelize) {
    /*
      Add reverting commands here.
      Return a promise to correctly handle asynchronicity.

      Example:
      return queryInterface.bulkDelete('Person', null, {});
    */
    return queryInterface.bulkDelete('TaskComponents', null, {});
  }
};
