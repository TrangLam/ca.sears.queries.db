const winston = require('winston');
const logger = new (winston.Logger)({
  transports: [
    // Put timestamp, colorize console text, out everthing in console.
    new (winston.transports.Console)({
      colorize: true,
      timestamp: true,
      level: 'debug'
    })
    // Can choose to add into output file later on.
  ]
});
const filename = 'loggers';


module.exports = {
  /*
   * Logging for any info related messages.
   *
   * @param filename Name of the file where this method is invoked.
   * @param method   Name of the method this logger in invoked from.
   */
  infoLogger: function(filename, method, message) {
    logger.info(`${filename} ${method} -- MESSAGE --`, message);
  },

  /*
   * Any info you like to log out for debugging purposes.
   *
   * @param filename Name of the file where this method is invoked.
   * @param method   Name of the method this logger in invoked from.
   * @param message  Debug message to display.
   */
  debugLogger: function(filename, method, message) {
    logger.debug(`${filename} ${method} -- DEBUG --`, message);
  },

  /*
   * When any task has been completed, use this method.
   *
   * @param filename Name of the file where this method is invoked.
   * @param method   Name of the method this logger in invoked from.
   * @param result   Optional result message if it exists.
   */
  doneLogger: function(filename, method, result = null) {
    if (result)
      logger.info(`${filename} ${method} -- COMPLETE --`, result);
    else
      logger.info(`${filename} ${method} -- COMPLETE`);
  },

  /*
   * When an error needs to be logged.
   *
   * @param filename Name of the file where this method is invoked.
   * @param method   Name of the method this logger in invoked from.
   * @param error    Error object/message.
   * @param message  Optional additional error message for debugging purposes.
   */
  errorLogger: function(filename, method, error, message = null) {
    if (message)
      logger.error(`${filename} ${method} -- MESSAGE: ${message}\n`, error);
    else
      logger.error(`${filename} ${method}\n`, error);
  }
}