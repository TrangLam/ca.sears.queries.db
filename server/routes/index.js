// Controller
const taskController = require('../controllers').mainTasks;
// JWT dependencies
const jwt = require('express-jwt');
const jwksRsa = require('jwks-rsa');
const jwtConfig = require('../config/jwtConfig.json');
const jwtAuthz = require('express-jwt-authz');

// Authentication middleware
const checkJwt = jwt({
  secret: jwksRsa.expressJwtSecret({
    cache: true,
    rateLimit: true,
    jwksRequestsPerMinute: 5,
    jwksUri: jwtConfig.jwksUri
  }),

  audience: jwtConfig.audience,
  issuer: jwtConfig.issuer,
  algorithms: jwtConfig.algorithms
});


const readScope = jwtAuthz(['read:allqueries']);
const editScope = jwtAuthz(['edit:query']);
const deleteScope = jwtAuthz(['delete:queries']);
const addScope = jwtAuthz(['add:query']);

module.exports = (app) => {
  // Create new task
  app.post('/tasks/', checkJwt, addScope, taskController.handleCreateTask);

  // Get tasks of profile
  // If :profile is 'all', then get all tasks from all profiles
  app.get('/tasks/:profile', checkJwt, readScope, taskController.handleListTasks);

  // Edit task
  app.put('/tasks/:id', checkJwt, editScope, taskController.handleUpdateTask);

  // Delete tasks
  app.delete('/tasks', checkJwt, deleteScope, taskController.handleDeleteTasks);

  // Get all task components
  app.get('/components', checkJwt, readScope, taskController.handleGetAllComponents);
};