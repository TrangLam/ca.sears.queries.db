'use strict';
module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.createTable('TaskComponents', {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: Sequelize.INTEGER
      },
      profileId: {
        type: Sequelize.INTEGER,
        onDelete: 'CASCADE',
        references: {
          model: 'TaskProfiles',
          key: 'id'
        }
      },
      label: {
        type: Sequelize.STRING,
        allowNull: false,
        validate : {
          notEmpty: true
        }
      },
      type: {
        type: Sequelize.STRING,
        allowNull: false,
        validate : {
          notEmpty: true
        }
      },
      required: {
        type: Sequelize.BOOLEAN,
        allowNull: false,
        validate : {
          notEmpty: true
        }
      },
      allowEdit: {
        type: Sequelize.BOOLEAN,
        allowNull: false,
        validate : {
          notEmpty: true
        }
      },
      createdAt: {
        allowNull: false,
        type: Sequelize.DATE
      },
      updatedAt: {
        allowNull: false,
        type: Sequelize.DATE
      }
    });
  },
  down: (queryInterface, Sequelize) => {
    return queryInterface.dropTable('TaskComponents');
  }
};