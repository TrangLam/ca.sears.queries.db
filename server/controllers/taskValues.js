const models = require('../models');
const TaskValue = require('../models').TaskValue;
const Task = require('../models').Task;
const TaskComponent = require('../models').TaskComponent;
const TaskProfile = require('../models').TaskProfile;

module.exports = {
  /**
   * For each value, insert to TaskValue table
   *
   * @param values
   * @return {Promise.<*>}
   */
  addValues(values) {
    const promises = [];
    values.forEach((value) => {
      promises.push(
        TaskValue.create(value, {
          fields: Object.keys(value)
        })
      );
    });

    return Promise.all(promises);
  },

  /**
   * From provided taskId, get all values
   *
   * @param taskId
   * @return {Promise.<Array.<Model>>}
   */
  getValuesFromTaskId(taskId) {
    return TaskValue.findAll({
      where: {
        taskId
      }
    });
  },

  /**
   * Join TaskValue with TaskComponent,
   * then inner join with (inner join of
   * TaskProfile and Task, where TaskProfile = profile)
   *
   * @param profile
   * @return {Promise.<Array.<Model>>}
   */
  getValuesFromProfile(profile) {
    return TaskValue.findAll({
      include: [
        {
          model: TaskComponent,
          attributes: ['label']
        },
        {
          model: Task,
          attributes: ['id', 'name'],
          include: {
            model: TaskProfile,
            attributes: ['profile'],
            where: { profile }
          },
          required: true,  // Convert to inner join,
        }
      ],
      attributes: ['value'],
    })
  },

  /**
   * Find TaskValue by taskId and componentId,
   * Then update TaskValue with value
   *
   * @param data {taskId, componentId, value}
   * @return {Promise}
   */
  updateValue(data) {
    return new Promise((resolve, reject) => {
      // Find TaskValue by taskId and componentId
      TaskValue.find({  // todo - check if UpdatedAt is updated
        where: {
          taskId: data.taskId,
          componentId: data.componentId
        }
      })
      .then((valueInstance) => {
        // If cannot find a value instance,
        // reject with Task Not Found
        if (!valueInstance) {
          reject(new Error('Task Not Found'));
        }

        // Update valueInstance with value in data
        return valueInstance.update({
          value: data.value
        }, {
          fields: ['value']
        });
      })
      .then((updatedInstance) => {
        resolve(updatedInstance);
      })
      .catch((error) => {
        reject(error);
      })
    });
  }
};