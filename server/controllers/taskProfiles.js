const TaskProfile = require('../models').TaskProfile;
const logger = require('../utils/logger');
const filename = 'taskProfiles';

module.exports = {
  getId(profile) {
    return new Promise((resolve, reject) => {
      TaskProfile
        .find({
          where: { profile: profile },
          attributes: ['id']
        })
        .then((result) => {
          const id = result.id;
          resolve(id);
        })
        .catch((error) => {
          logger.errorLogger(filename, 'getId', error);
          reject(error);
        })
    });
  },

  getAllProfiles() {
    return TaskProfile.findAll({
        attributes: ['profile']
      })
      .then((result) => {
        const profiles = result.map((item) => {
          return item.profile;
        });

        return profiles;
      })
      .catch((error) => {
        return error;
      })
  }
};