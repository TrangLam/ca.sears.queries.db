const TaskComponent = require('../models').TaskComponent;
const TaskProfile = require('../models').TaskProfile;
const logger = require('../utils/logger');
const filename = 'taskComponents';

module.exports = {
  getComponentLabelIdFromProfileId(id) {
    return new Promise((resolve, reject) => {
      TaskComponent
        .findAll({
          where: {
            profileId: id
          }
        })
        .then((result) => {
          const components = result.map((item) => {
            return {
              id: item.id,
              label: item.label
            };
          });
          logger.doneLogger(filename, 'getComponentLabelIdFromProfileId', components);
          resolve(components);
        })
        .catch((error) => {
          logger.errorLogger(filename, 'getId', error);
          reject(error);
        })
    });
  },

  getComponentIds(labels, profileId) {
    return TaskComponent
      .findAll({
        where: {
          profileId,
          label: {
            in: labels
          }
        },
        attributes: ['id', 'label']
      })
  },

  getAllComponentsByProfile() {
    return new Promise((resolve, reject) => {
      TaskComponent
        .findAll({
          include: [  // join with TaskProfile
            {
              model: TaskProfile,
              attributes: ['profile']
            }
          ]
        })
        .then((components) => {
          const componentsByProfiles = {};

          components.forEach((component) => {
            const { label, type, required, allowEdit } = component;
            const profile = component.TaskProfile.profile;

            // Create array if not exist
            if (!componentsByProfiles.hasOwnProperty(profile)) {
              componentsByProfiles[profile] = [];
            }

            // Add component to array
            componentsByProfiles[profile].push({
              label,
              type,
              required,
              allowEdit
            });
          });

          resolve(componentsByProfiles);
        })
        .catch((error) => {
          logger.errorLogger(filename, 'getAllComponentsByProfile', error);
          reject(error);
        })
    });
  }
};