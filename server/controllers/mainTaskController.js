const taskController = require('./tasks');
const valueController = require('./taskValues');
const profileController = require('./taskProfiles');
const componentController = require('./taskComponents');
const logger = require('../utils/logger');
const _ = require('lodash');
const filename = 'tasks';

//////////////////////////////
////  HELPER FUNCTIONS     //
////////////////////////////

/**
 * Get profileId and its components
 *
 * @param profile
 * @return {Promise}
 */
function getProfileComponents(profile) {
  let profileId;
  return new Promise((resolve, reject) => {
    profileController.getId(profile)
      .then((resultProfileId) => {
        profileId = resultProfileId;
        return componentController.getComponentLabelIdFromProfileId(profileId);
      })
      .then((components) => {
        const result = {
          profileId,
          components,
        };
        console.log('finished getComponentLabelIdFromProfileId', result); // todo - remove
        resolve(result);
      })
      .catch((error) => {
        logger.errorLogger(filename, 'getProfileComponents', error);
        reject(error);
      })
  });
}

/**
 * Combine componentId with value in taskProperties
 *
 * @param components
 * @param taskProperties
 * @param taskId
 * @return {Array|*}
 */
function prepareComponentIdsValues(components, taskProperties, taskId) {
  const values = components.map((component) => {
    return {
      taskId: taskId,
      componentId: component.id,
      value: taskProperties[component.label],
      createdAt: new Date(),
      updatedAt: new Date()
    }
  });

  return values;
}

/**
 * Helper to get tasks from a profile
 *
 * @param profile
 * @return {Promise} { profile: [] }
 */
function listTasksHelper(profile) {
  return new Promise ((resolve, reject) => {
    valueController.getValuesFromProfile(profile)
      .then((result) => {
        let tasksObj = {};

        // For each value item, add to taskObj
        result.forEach((item) => {
          const name = item.Task.name;
          const id = item.Task.id;
          const label = item.TaskComponent.label;

          // Create new task object if not exists
          if (!tasksObj.hasOwnProperty(name)) {
            tasksObj[name] = {
              id,
              name
            }
          }

          // Add task value
          tasksObj[name][label] = item.value;
        });

        const tasks = {};
        tasks[profile] = _.values(tasksObj);

        logger.doneLogger(filename, 'listTasksHelper', tasks);
        resolve(tasks);
      })
      .catch((error) => {
        logger.errorLogger(filename, 'listTasksHelper', error);
        reject(error);
      })
  });
}

//////////////////////////////
////  REQUEST HANDLERS     //
////////////////////////////

/**
 * Get profile id, and component labels
 * then add task,
 * then add task value
 *
 * @param req
 * @param res
 * @return {Promise}
 */
function handleCreateTask(req, res) {
  const { profile, name, taskProperties } = req.body;
  let profileId;
  let taskId;
  let components;

  return getProfileComponents(profile)
    .then((profileComponents) => {
      profileId = profileComponents.profileId;
      components = profileComponents.components;

      // Insert task with name and profileId to table Tasks
      return taskController.createTask(name, profileId);
    })
    .then((resultTask) => {
      taskId = resultTask.id;
      // Prepare values
      const values = prepareComponentIdsValues(components, taskProperties, taskId);

      console.log('finished prepareComponentIdsValues', values);  // todo - remove
      // Insert values to table TaskValues
      return valueController.addValues(values);
    })
    .then(() => {
      const message = `Task ${name} has been created`;
      logger.doneLogger(filename, 'createTask', message);
      res.status(200).send({
        message: message
      });
    })
    .catch((error) => {
      logger.errorLogger(filename, 'createTask', error);
      res.status(400).send(error);
    })
}

/**
 * Get profile from request params,
 * then query database for the list of tasks
 * of the profile.
 *
 * profileParam can be 'all' or just one specific profile
 *
 * If profile === 'all', get all tasks
 *
 * @param req
 * @param res
 * @return {Promise.<TResult>}
 * {
 *   profile1: [],
 *   profile2: []
 * }
 */
function handleListTasks(req, res) {
  const profileParam = req.params.profile;
  const profilePromise = [];

  // Get profile(s) based on profileParam in API URL
  if (profileParam === 'all') {   // if param is all, get all profiles
    profilePromise.push(profileController.getAllProfiles());
  } else {  // else resolve to param
    profilePromise.push(Promise.resolve([profileParam]));
  }

  return Promise.all(profilePromise)
    .then((profiles) => {
      // Get tasks from all profiles returned
      const taskPromises = profiles[0].map((profile) => {
        return listTasksHelper(profile);
      });

      return Promise.all(taskPromises);
    })
    .then((tasks) => {
      // Reduce tasks array to object
      const result = _.reduce(tasks, (accumulated, profile) => {
        const key = _.keys(profile)[0];
        accumulated[key] = profile[key];
        return accumulated;
      }, {});

      // Respond to client
      res.status(200).send(result);
    })
    .catch((error) => {
      logger.errorLogger(filename, 'handleListTasks', error);
      res.status(400).send(error);
    });
}

/**
 *  Get profileId with taskId from Tasks table
 *  then get ComponentId from TaskComponents table,
 *  then update value
 *
 * @param req (req.body: {id: id, label1: value1, label2: value2})
 * @param res
 * @return {Promise.<TResult>}
 */
function handleUpdateTask(req, res) {
  const changes = req.body;
  const id  = req.params.id;

  // Get profileId from Task table with taskId
  return taskController.getProfileIdFromTaskId(id)
    .then((returnedTask) => {
      const labels = _.keys(changes);

      // Get componentIds for all properties in changes object with profileId
      return componentController.getComponentIds(labels, returnedTask.profileId);
    })
    .then((components) => {
      // For each component returned, update value in TaskValue
      const promises = components.map((component) => {
        const data = {
          taskId: id,
          componentId: component.id,
          value: changes[component.label]
        };

        return valueController.updateValue(data);
      });

      return Promise.all(promises);
    })
    .then((updatedTaskValue) => {
      res.status(200).send(updatedTaskValue);
    })
    .catch((error) => {
      logger.errorLogger(filename, 'handleUpdateTask', error);
      res.status(400).send(error);
    });
}

/**
 * For each id in ids, destroy task
 *
 * @param req
 * @param res
 * @return {Promise.<TResult>}
 */
function handleDeleteTasks(req, res) {
  const ids = req.body.ids;
  const promises = [];

  ids.forEach((id) => {
    promises.push(taskController.destroyTask(id))
  });

  return Promise.all(promises)
    .then((deletedIds) => {
      res.status(200).send({
        message: `Queries ${deletedIds.join(', ')} deleted successfully.`
      })
    })
    .catch(error => res.status(400).send(error));
}

/**
 * Get All Components in TaskComponents table,
 * categorized by Task Profile
 *
 * @param req
 * @param res
 * @return {Promise.<TResult>}
 */
function handleGetAllComponents(req, res) {
  return componentController.getAllComponentsByProfile()
    .then((result) => {
      res.status(200).send(result);
    })
    .catch((error) => {
      logger.errorLogger(filename, 'handleGetComponents', error);
      res.status(400).send(error);
    });
}

module.exports = {
  handleCreateTask : handleCreateTask,
  handleListTasks: handleListTasks,
  handleUpdateTask: handleUpdateTask,
  handleDeleteTasks: handleDeleteTasks,
  handleGetAllComponents: handleGetAllComponents
};