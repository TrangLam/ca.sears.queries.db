const Task = require('../models').Task;

/**
 * Insert an instance to Task Model
 *
 * @param name
 * @param profileId
 * @return {value}
 */
function createTask(name, profileId) {
  const value = {
    name,
    profileId
  };

  console.log('createTask value', value); // todo - remove
  return Task.create(value);
}

function getTaskById(id) {
  return Task.findById(id);
}

function getTasksFromProfile(profileId) {
  return Task.findAll({
    where: {
      profileId
    }
  });
}

function getProfileIdFromTaskId(id) {
  return Task.find({
    where: {
      id
    },
    attributes: ['profileId']
  })
}

function destroyTask(id) {
  return getTaskById(id)
    .then((task) => {
      if (!task) {
        throw new Error(`Cannot find task with id ${id} to delete`);
      } else {
        return task.destroy();
      }
    })
    .then(() => {
      return id;
    })
    .catch((error) => {
      throw error;
    });
}

module.exports = {
  createTask: createTask,
  getTasksFromProfile: getTasksFromProfile,
  getProfileIdFromTaskId : getProfileIdFromTaskId,
  destroyTask: destroyTask
};