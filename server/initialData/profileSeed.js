const taskProfiles = [
  {
    profile: 'dbRelated'
  },
  {
    profile: 'sftpLocal'
  },
  {
    profile: 'localS3'
  },
  {
    profile: 's3Redshift'
  },
  {
    profile: 'logfireTransferXML'
  },
  {
    profile: 'logfireTransferDownload'
  }
];

module.exports = {
  taskProfiles: taskProfiles
};