const Components = {
  dbRelated: [
    {
      label: 'type',
      type: 'input',
      required: true,
      allowEdit: true,
    },
    {
      label: 'description',
      type: 'textarea',
      required: false,
      allowEdit: true,
    },
    {
      label: 'source_db',
      type: 'input',
      required: true,
      allowEdit: true,
    },
    {
      label: 'dest_db',
      type: 'input',
      required: true,
      allowEdit: true,
    },
    {
      label: 'dest_table',
      type: 'input',
      required: true,
      allowEdit: true,
    },
    {
      label: 'query',
      type: 'textarea',
      required: true,
      allowEdit: true,
    },
    {
      label: 'rollback',
      type: 'textarea',
      required: true,
      allowEdit: true,
    },
  ],
  sftpLocal: [
    {
      label: 'type',
      type: 'input',
      required: true,
      allowEdit: true,
    },
    {
      label: 'description',
      type: 'textarea',
      required: false,
      allowEdit: true,
    },
    {
      label: 'sftp_dir',
      type: 'input',
      required: true,
      allowEdit: true,
    },
    {
      label: 'source_file',
      type: 'input',
      required: true,
      allowEdit: true,
    },
    {
      label: 'target_file_path',
      type: 'input',
      required: true,
      allowEdit: true,
    },
    {
      label: 'sftp_id',
      type: 'input',
      required: true,
      allowEdit: true,
    },
  ],
  localS3: [
    {
      label: 'type',
      type: 'input',
      required: true,
      allowEdit: true
    },
    {
      label: 'description',
      type: 'textarea',
      required: false,
      allowEdit: true
    },
    {
      label: 's3_bucket',
      type: 'input',
      required: true,
      allowEdit: true
    },
    {
      label: 'source_file_path',
      type: 'input',
      required: true,
      allowEdit: true
    },
    {
      label: 's3_target_file_path',
      type: 'input',
      required: true,
      allowEdit: true
    },
  ],
  s3Redshift: [
    {
      label: 'type',
      type: 'input',
      required: true,
      allowEdit: true
    },
    {
      label: 'description',
      type: 'textarea',
      required: false,
      allowEdit: true
    },
    {
      label: 'source_file_path',
      type: 'input',
      required: true,
      allowEdit: true
    },
    {
      label: 'sql',
      type: 'textarea',
      required: true,
      allowEdit: true
    },
    {
      label: 'rollback',
      type: 'textarea',
      required: true,
      allowEdit: true
    },
  ],
  logfireTransferXML: [
    {
      label: 'type',
      type: 'input',
      required: true,
      allowEdit: true
    },
    {
      label: 'localDir',
      type: 'input',
      required: true,
      allowEdit: true
    },
    {
      label: 'batchSize',
      type: 'input',
      required: true,
      allowEdit: true
    },
  ],
  logfireTransferDownload: [
    {
      label: 'type',
      type: 'input',
      required: true,
      allowEdit: true
    },
    {
      label: 'localDir',
      type: 'input',
      required: true,
      allowEdit: true
    },
    {
      label: 'remoteDir',
      type: 'input',
      required: true,
      allowEdit: true
    },
    {
      label: 'sftp_connection_name',
      type: 'input',
      required: true,
      allowEdit: true
    }
  ]
};

module.exports = {
  Components: Components
};